import { DetailReducers } from "./DetailReducers";
import { createStore, combineReducers } from "redux";

// export const store = createStore(DetailReducers, {});
const reducer = combineReducers({
    DetailReducers : DetailReducers,
});
 const store = createStore(reducer);

 export default store;
