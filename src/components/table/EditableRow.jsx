import React from 'react'

const EditableRow = ({editFormData,setEditFormDatas,Cancelbtn}) => {
  return (
    <tr>
      <td>
      <input
          type="text"
          required="required"
          placeholder="Enter a update Companyname..."
          name="CompanyName"
          value={editFormData.CompanyName}
          onChange={e=>setEditFormDatas({...editFormData, CompanyName:e.target.value })}
         
        ></input>
      </td>
      <td>
      <input
          type="address"
          required="required"
          placeholder="Enter update address..."
          name="address"
          value={editFormData.address}
          onChange={e=>setEditFormDatas({...editFormData, address:e.target.value })}
        ></input>
      </td>
      <td>
      <input
          type="number"
          required="required"
          placeholder="Enter update employee num..."
          name="Employees"
          value={editFormData.Employees}
          onChange={e=>setEditFormDatas({...editFormData, Employees:e.target.value })}
          
        ></input>
      </td>
      <td>
      <input
          type="identity"
          required="required"
          placeholder="Enter update employee num..."
          name="International"
          value={editFormData.International}
          onChange={e=>setEditFormDatas({...editFormData, International:e.target.value })}
         
        ></input>
      </td>
      <td>
        <button type="submit">Save</button>
        test
        <button type="button" onClick={Cancelbtn}>
          Cancel
        </button>
      </td>
    </tr>
  )
}

export default EditableRow