// import React from 'react';
// import ReactDOM from 'react-dom/client';
// import './index.css';
// import App from './App';
// import reportWebVitals from './reportWebVitals';
// import { Provider } from 'react-redux';
// // import { store } from './components/table/store';
// import { DetailReducers } from "../src/components/table/DetailReducers";
// import { createStore } from "redux";
// import store from './components/table/store';

// // const store = createStore(DetailReducers, {});

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(
//   // <App />
  
  
//   <React.StrictMode>
//    <Provider store={store}>
//    <App />
//     </Provider> 
//   </React.StrictMode>
//   root
// );

// // If you want to start measuring performance in your app, pass a function
// // to log results (for example: reportWebVitals(console.log))
// // or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();

import React from "react";
import ReactDOM from "react-dom";
// import { BrowserRouter as Router } from "react-router-dom";

import App from "./App";
// import "bootstrap/dist/css/bootstrap.min.css";

// import "react-toastify/dist/ReactToastify.css";

import { createStore } from "redux";
import { Provider } from "react-redux";
// import { contactReducer } from "./redux/reducers/contactReducer";
// import { composeWithDevTools } from "redux-devtools-extension";
import { DetailReducers } from "./components/table/DetailReducers";

const store = createStore(DetailReducers);

const rootElement = document.getElementById("root");
ReactDOM.render(
  <Provider store={store}>
      <App />
  </Provider>,
  rootElement
);
