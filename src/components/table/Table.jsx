import React, { Fragment } from 'react'
import './Table.css';
import Records from '../Records.json';
import { nanoid } from "nanoid";
import ReadOnlyRow from './ReadOnlyRow';
import EditableRow from './EditableRow';
import { connect, useDispatch, useSelector } from 'react-redux';
import { DetailReducers } from './DetailReducers';


const Table = ({ contacts, addContact ,updateContact}) => {

  // const select = useSelector((state)=>{
  //   return state;
  // });
  // const dispatch = useDispatch();
  // const selector = useSelector();
  // console.log('test test test agian' , select)

  // const [detail, setDetail] = React.useState([])
  const [addformdata, setAddformData] = React.useState({
    CompanyName: '',
    address: '',
    Employees: '',
    International: '',
  })

  const handleAddFormSubmit = (event) => {
    event.preventDefault();

    const newContact = {
      id: nanoid(),
      CompanyName: addformdata.CompanyName,
      address: addformdata.address,
      Employees: addformdata.Employees,
      International: addformdata.International,
    };

    addContact(newContact);


  const [editFormData, setEditFormData] = React.useState({
    CompanyName: '',
    address: '',
    Employees: '',
    International: '',
  });

  
  
  const [editdetailId, setEditDetailId] = React.useState(null);
    // const newContacts = [...detail, newContact];
    // setDetail(newContacts);
  };

  const handleEditClick = (event, contact) => {
    event.preventDefault();
    setEditDetailId(contact.id)
  }

  const handleEditFormSubmit = (event) => {
    event.preventDefault();

    const editeddetail = {
      id: editdetailId,
      CompanyName: editFormData.CompanyName,
      address: editFormData.address,
      Employees: editFormData.Employees,
      International: editFormData.International,
    };

    const newdetails = [...contacts];

    const index = contacts.findIndex((contact) => contact.id === editdetailId);

    newdetails[index] = editeddetail;

    updateContact(editeddetail)
    // setEditDetailId(null);
  }

  // const handleCancelClick = () => {
  //   setEditDetailId(null)
  // }

  // const handleDeleteClick = (detailId) => {

  //   const newDetail = [...detail];
  //   const index = detail.findIndex((details) => details.id === detailId);

  //   newDetail.splice(index, 1);
  //   setDetail(newDetail);
  // }

  // const [value, setValue] =React.useState("");
  // const [dataSource ,SetDataSource]= React.useState(detail);
  // const [tableFltr, setTableFltr]= React.useState([]);
  // const filterData=(e)=>{
  //   if(e.target.value !== "" ){
  //     setValue(e.target.value);
  //     const filterTable = dataSource.filter(o=>Object.keys(o).some(k=>String(o[k]).toLowerCase().includes(e.target.value.toLowerCase()
  //     )));
  //     setTableFltr(...filterTable)

  //   }else{
  //     setValue(e.target.value);
  //     SetDataSource([...dataSource])
  //   }
  // }


  return (
    <div className='.app-container' onSubmit={handleAddFormSubmit}>
      <div className='filterBox'>

        <input className='serchBox' type="text" name="" id="" placeholder='searchbox'
        />
        <div className='textSearchBox'> <h4>Search box</h4></div>
      </div>
      
      <form action="" onSubmit={handleEditFormSubmit} >
        <table>
          <thead>
            <tr>
              <th>Company</th>
              <th>Address</th>
              <th>Employees</th>
              <th>International</th>
              <th>Actions</th>
            </tr>
          </thead>

          <tbody>
            {(contacts.map((contact, id) => (

              
              // <ReadOnlyRow />

              //  <Fragment>
              //     {editdetailId === detail.id ? (
              //       <EditableRow
              //         editFormData={editFormData}
              //         setEditFormDatas={setEditFormData}
              //         Cancelbtn={handleCancelClick}
              //       />
              //     )
              //       : (<ReadOnlyRow details={Detail}
              //         handleEditClick={handleEditClick}
              //         DeleteBtn={handleDeleteClick} />
              //       )
              //     }
              //   </Fragment>

              <Fragment>
              {editdetailId === contact.id ? (
                <EditableRow
                  editFormData={editFormData}
                  setEditFormDatas={setEditFormData}
                  // Cancelbtn={handleCancelClick}
                />
              )
                : (<ReadOnlyRow 
                  contact={contact} id={id}
                  handleEditClick={handleEditClick}
                  // DeleteBtn={handleDeleteClick} 
                  />
                )
              }
            </Fragment>

            )))}
          </tbody>
        </table>
      </form>

      <h2>Add the detail</h2>

      <form action="" className='adddetailForm' onSubmit={handleAddFormSubmit}>
        <input
          type="text"
          name='CompanyName'
          // required="required"
          placeholder='Enter company name...'
          onChange={e => setAddformData({ ...addformdata, CompanyName: e.target.value })} />

        <input
          type="address"
          name='address'
          // required="required"
          placeholder='Enter adress ...'
          onChange={e => setAddformData({ ...addformdata, address: e.target.value })} />


        <input
          type="number"
          name='Employees'
          // required="required"
          placeholder='Enter employee no...'
          onChange={e => setAddformData({ ...addformdata, Employees: e.target.value })} />


        <input
          type="identity"
          name='International'
          // required="required"
          placeholder='is it international true or false...'
          onChange={e => setAddformData({ ...addformdata, International: e.target.value })} />

        <button onClick={handleAddFormSubmit}>Add detail</button>
      </form>
    </div>
  )
}

// export default Table
const mapStateToProps = (state) => ({
  contacts: state,
});

const mapDispatchToProps = (dispatch) => ({
  addContact: (newContact) => {
    dispatch({ type: "ADD_CONTACT", payload: newContact });
  },

  updateContact: (editeddetail) => {
    dispatch({ type: "UPDATE_CONTACT", payload: editeddetail });
  },
});



// function mapStateToProps(state) {
//   return   {todos : state}
//   //  { todoList: todos }
// }

export default connect(mapStateToProps, mapDispatchToProps)(Table);