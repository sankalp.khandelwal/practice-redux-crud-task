import React from 'react'

const ReadOnlyRow = ({contact, handleEditClick, DeleteBtn, id}) => {
    return (
        <tr key={id}>
            <td>{contact.CompanyName}</td>
            <td>{contact.address}</td>
            <td>{contact.Employees}</td>
            <td>{contact.International}</td>
            <td>
            <button
                type="button"
                onClick={(event) => handleEditClick(event, contact)}
            >
                Edit
            </button>
            <button type="button"
             onClick={() => DeleteBtn(contact.id)}
             >
                Delete
            </button>
        </td>
          </tr >
  )
}

export default ReadOnlyRow